export class Producto{
    constructor(
        public name:string,
        public price:string,
        public description:string,
        public image:string
    ){}
}