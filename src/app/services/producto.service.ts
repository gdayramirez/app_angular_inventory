import { Injectable } from '@angular/core'
import { Http, Headers } from '@angular/http'
import 'rxjs/add/operator/map';
import { Producto } from '../models/producto'
import { Global } from '../services/global'

@Injectable()

export class ProductoService {
    public url: string;
    constructor(
        public _http: Http
    ) {
        this.url = Global.url
    }
    editProducto(id, producto: Producto) {
        let json = JSON.stringify(producto)
        let params = json
        let headers = new Headers({
            "Content-Type": 'application/json'
        })
        return this._http.put(this.url + '/update/' + id, params, { headers: headers }).map(
            res => res.json()
        )
    }

    deleteProducto(id) {
        return this._http.delete(this.url + '/delete/' + id).map(res => res.json())
    }
    getProductos() {
        return this._http.get(this.url + '/get').map(res => res.json())
    }
    getProducto(id) {
        return this._http.get(this.url + '/get/' + id).map(res => res.json())
    }
    addProducto(producto: Producto) {

        let json = JSON.stringify(producto);
        let params = json;
        let headers = new Headers({
            "Content-Type": 'application/json'
        })
        console.log(this.url + '/create/product/')
        return this._http.post(this.url + '/create/product/', params, { headers: headers }).map(
            res => res.json()
        )
    }
    makeFileRequest(
        url: string,
        files: Array<File>
    ) {
        return new Promise((resolve, rejected) => {
            var formData = new FormData()
            var xhr = new XMLHttpRequest()
            for (var i = 0; i <= files.length; i++) {
                formData.append('file', files[0], files[0].name)
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response))
                    }
                    else {
                        rejected(xhr.response)
                    }
                }
            }
            xhr.open('POST', url, true)
            xhr.send(formData)
        })
    }
}