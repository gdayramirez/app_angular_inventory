import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ProductoService } from '../services/producto.service'
import { Producto } from '../models/producto'
import { Global } from "../services/global";
@Component({
    selector: 'producto-add',
    templateUrl: '../views/producto-add.html',
    providers: [
        ProductoService
    ]
})

export class ProductoAddComponent {
    public titulo;
    public producto: Producto;
    public _error;
    public filesToUpload
    public resultUpload
    constructor(
        private _productoService: ProductoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.titulo = 'Crear un nuevo producto'
        this.producto = new Producto('', '', '', '');
    }

    ngOnInit() {
        console.log('Producto add component cargado')
    }
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        console.log(this.filesToUpload)
    }
    onSubmit() {
        console.log(this.producto)
        this._productoService.makeFileRequest(Global.url + '/upload/image/', this.filesToUpload)
            .then(
                (result) => {
                    this.resultUpload = result
                    this.producto.image = this.resultUpload.data.Location;
                    this.save()
                },
                (error) => {
                    console.log(error)
                }
            )
    }
    save() {
        this._productoService.addProducto(this.producto).subscribe(
            response => {
                this._router.navigate(['/productos'])
            },
            error => {
                this._error = error._body ? error._body : ''
                console.log(this._error)
            }
        )
    }
}
