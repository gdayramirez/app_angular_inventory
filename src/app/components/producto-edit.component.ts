import { Component } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { ProductoService } from '../services/producto.service'
import { Producto } from '../models/producto'
import { Global } from '../services/global'

@Component({
    selector: 'producto-edit',
    templateUrl: '../views/producto-add.html',
    providers: [
        ProductoService
    ]
})

export class ProductoEditComponent {
    public title: string;
    public producto: Producto;
    public filesToUpload
    public resultUpload
    public isEdit
    public _error: string
    constructor(
        private _productoService: ProductoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        this.title = 'Editar Producto'
        this.producto = new Producto('', '', '', '')
        this.isEdit = true
    }

    ngOnInit() {
        console.log(this.title)
        this.getProducto()
    }

    getProducto() {
        console.log('AQUI ANDO')
        this._route.params.forEach((params: Params) => {
            let id = params['id']
            this._productoService.getProducto(id).subscribe(success => {
                this.producto = success.data
                console.log(this.producto)
            }, error => {
                console.log(<any>error)
            })
        })
    }
    /**
     * 
     * @param {File} fileInput file loaded on file input 
     */
    fileChangeEvent(fileInput: any) {
        this.filesToUpload = <Array<File>>fileInput.target.files;
        console.log(this.filesToUpload)
    }
    onSubmit() {
        console.log(this.producto)
        if (this.filesToUpload && this.filesToUpload.length >= 1)
            this._productoService.makeFileRequest(Global.url + '/upload/image/', this.filesToUpload)
                .then(
                    (result) => {
                        this.resultUpload = result
                        this.producto.image = this.resultUpload.data.Location;
                        this.update()
                    },
                    (error) => {
                        console.log(error)
                    }
                )
        else
            this.update()
    }
    update() {
        this._route.params.forEach((params: Params) => {
            let id = params['id']
            this._productoService.editProducto(id, this.producto).subscribe(
                response => {
                    this._router.navigate(['/producto', id])
                },
                error => {
                    this._error = error._body ? error._body : ''
                    console.log(this._error)
                }
            )
        })
    }
}