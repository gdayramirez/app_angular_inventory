import { Component } from '@angular/core'
import { ProductoService } from '../services/producto.service'
import { Producto } from '../models/producto'

@Component({

    selector: 'productos-list',
    templateUrl: '../views/productos-list.html',
    providers: [
        ProductoService
    ]
})

export class ProductosListComponent {
    public titulo: String;
    public productos: Producto
    constructor(
        private _productoService: ProductoService
    ) {
        this.titulo = 'Listado de productos'
    }

    ngOnInit() {
        console.log('En listado')
        this.getProductos()
    }
    getProductos() {
        this._productoService.getProductos().subscribe(
            result => {
                console.log(result.data)
                this.productos = result.data;
            },
            error => {
                console.log(<any>error)
            }
        )
    }

    onDeleteProducto(id) {
        this._productoService.deleteProducto(id).subscribe(result => {
            this.getProductos()
        }, error => {
            console.log(<any>error)
        })
    }
}