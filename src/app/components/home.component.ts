import { Component } from '@angular/core'
@Component({
    selector: 'home',
    templateUrl: '../views/home.html'
})

export class HomeComponent {
    public titulo: string;
    constructor() {
        this.titulo = "Inventario de productos";
    }
    ngOnInit() {
        console.log('Home component iniciado')
    }
}