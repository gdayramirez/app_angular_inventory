import { Component } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router'
import { ProductoService } from '../services/producto.service'
import { Producto } from '../models/producto'
@Component({
    selector: 'producto-detail',
    templateUrl: '../views/producto-detail.html',
    providers: [
        ProductoService
    ]
})
export class ProductoDetailComponent {
    public producto: Producto;
    constructor(
        private _productoService: ProductoService,
        private _route: ActivatedRoute,
        private _router: Router
    ) { }
    ngOnInit() {
        console.log('producto - detalle . component ')
        this.getProducto()
    }
    getProducto() {
        console.log('AQUI ANDO')
        this._route.params.forEach((params: Params) => {
            let id = params['id']
            this._productoService.getProducto(id).subscribe(success => {
                this.producto = success.data
                console.log(this.producto)
            }, error => {
                console.log(<any>error)
            })
        })
    }
}